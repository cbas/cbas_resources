![](CBAS.jpg)

# CBAS Resources

This is an umbrella site that hosts the data we generate on **CBAS**. It contains pointers to other repositories with data on this system.

Note that these other repositories are mantained independently and that this repository uses git **submodules** to pull those repositories and the most recent changes pushed to them. This repository **should not be used to introduce changes to the submodules**, it was only created to centralize all the data generated on **CBAS**. If you want to contribute to one of the repositories, please send a pull request to that repository. The reason why I chose to provide this umbrella repository reflects the way in which the project develops. I think it is easier to develop independent repositories for specific projects and add these repos as submodules to this repository once the specific experiments are concluded, the data is available and is hopefully analyzed in some meaningful way.

The goal of this repository is thus to provide users with one single central hub that can be easily cloned with all the available information generated on **CBAS** at a certain point in time. I hope this makes sense, and it makes your lives easier.

cheers

Sergio

***

# Cloning instructions

Copy the url using the clone button on the main page of this repository and open a new terminal. If you want `git` to fetch and update all the submodules on cloning, you need to pass the `--recurse-submodules` flag to `git clone`. Otherwise you need to use the `git submodule init` followed by `git submodule update` commands to get the data stored in the submodule repositories.

## Current submodules

 - [CBAS_Transcriptome](https://gitlab.lrz.de/cbas/CBAS_Transcriptome): data and scripts used to 1. assemble **CBAS** reference transcriptome and 2. to assess gene expression changes during photo symbiont loss.
 - [CBAS_16S](https://gitlab.lrz.de/cbas/CBAS_16S): 16S V4 amplicon data. This repo groups data from different projects using 16S V4 amplicon sequencing. It includes our shading/bleaching experiment.
 - [CBAS_Metatranscriptome](https://gitlab.lrz.de/cbas/CBAS_Metatranscriptome): metatranscriptomic data for **CBAS** and *Tethya wilhelma*.
 - [CBAS_Pictures](https://gitlab.lrz.de/cbas/CBAS_Pictures): Macro and micro pictures of **CBAS** under control or shading (bleached sponges) conditions.

## Planned future submodules

 - CBAS_Metagenome: metagenomic data for **CBAS** including several bacterial assemblies. This is work in progress.
 - CBAS_Genome: eventually, with information on this species' genome.

## Experimental settings and detailed protocols used for different experiments

Another reason why I consider important to mantain this centralized repository is to keep somewhere a detailed as possible account on the experimental settings we use in our experiments. I have been trying to keep this as transparent as possible, but think that often more detail is necessary. The same happens with the protocols we use. I have optimized most of the through rounds of trial and error and too often journals do not accept long descriptions of procedures. 

***

# About **CBAS** 

**CBAS** is a blue/purple/green encrusting sponge that can be commonly found in marine aquaria all over the world. **CBAS** mantains a symbiotic relation with cyanobacteria of the Candidatus *Synechococcus spongiarum* group. In addition, other bacteria (at least 6 more) are present in its microbiome. The main objective of my research is to understand how this holobiont works. For this I mostly use *omic* methods (i.e. transcriptomics, (meta)genomics, etc). I think **CBAS** represents a unique model  system for the study of sponge-microbiome interactions because it can be easily investigated in different parts of the world.

## What does CBAS means and why do I keep finding it.

The story goes:

by the time I started working with **CBAS** its taxonomy was not clear. The only information I had about the sponge was:

 - it was blue
 - it can be found in many aquaria
 - in these systems is pretty common
 - is a sponge

from this information I came up with the acronym **C**ommon **Blue** **A**quarium **S**ponge (**CBAS**). I think this is a nice name and will keep using it for my research.

***

# People involved in the project(s):
- Sergio Vargas, LMU München (me)
- Michael Eitel, LMU München
- Gert Wörheide, LMU München
- Warren Francis, LMU München
- Laura Leiva (now at: SiTools, Munich)
- Fabian Kellner (now at: NTNU University Museum)

# Citing our work

If you use these resources please cite the following papers/preprints:

  1. Sergio Vargas, Laura Leiva, and Gert Wörheide. 2020. "Short-Term Exposure to High-Temperature Water Causes a Shift in the Microbiome of the Common Aquarium Sponge *Lendenfeldia chondrodes*." Microbial Ecology, August. https://doi.org/10.1007/s00248-020-01556-z.

  2. Sergio Vargas, Christopher Arnold, Michael Nickel and Gert Wörheide. 2021. "Host-mediated competition avoidance in a cyanosponge." Unpublished.

  3. Sergio Vargas, Laura Leiva Michael Eitel, Franziska Curdt, Sven Rohde, Christopher Arnold, Michael Nickel, Peter Schupp, William D. Orsi, Maja Adamska, Gert Wörheide. 2023. "Body-plan reorganization in a sponge correlates with microbiome change." Molecular Biology and Evolution 40 (6): msad138.

  4. Sergio Vargas, Ramón E. Rivera-Vicéns, Michael Eitel, Laura Leiva, Gabrielle Büttner, Gert Wörheide. 2024. "rRNA depletion for holobiont metatranscriptome profiling across demosponges". bioRxiv. https://www.biorxiv.org/content/early/2024/06/08/2022.08.12.503726

---


#### NOTES

1. This repository (or parts of it) is in active development. Check the releases section to see if there are snap-shots of it.

2. This repository is **public** and the following personal information is visible:
	* After each **commit*, the name and E-mail address as saved in your Git config.
	* The **GitLab username* as saved in your user settings (under Account).

3. In German: Bitte beachten Sie: Bei öffentlichen Projekten sind die folgenden Informationen öffentlich sichtbar (allgemein zugreifbar):
	* Bei jedem Commit der Name und die E-Mail-Adresse, wie sie in der Konfiguration von Git hinterlegt wurden.
	* Bei GitLab der GitLab-Benutzername (Username), wie er bei den Benutzereinstellungen (User Settings) im Abschnitt "Konto" (Account) festgelegt wird. Der Benutzername ist bei persönlichen Projekten im Pfad sichtbar.

#### Note that by commiting to this repository you accept the publication of the information detailed above. More information about public repositories can be find here: https://doku.lrz.de/display/PUBLIC/GitLab


Sergio


<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
